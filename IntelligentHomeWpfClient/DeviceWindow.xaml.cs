﻿using Devices;
using Devices.Abstractions;
using Devices.Implementations;
using System;
using System.Collections.Generic;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IntelligentHomeWpfClient
{
    /// <summary>
    /// Interaction logic for DeviceWindow.xaml
    /// </summary>
    public partial class DeviceWindow : Window
    {
        public DeviceBase Device { get; set; }

        public DeviceWindow(DeviceBase device)
        {
            Device = device;
            
            InitializeComponent();

            comboBoxColors.ItemsSource = Helper.GetAllColors();

            Controls.RowDefinitions[2].Height = new GridLength(0); // irangeable
            Controls.RowDefinitions[3].Height = new GridLength(0); // icolorable
            Controls.RowDefinitions[4].Height = new GridLength(0); // itwostatable

            sliderRange.Visibility = Visibility.Collapsed;
            comboBoxColors.Visibility = Visibility.Collapsed;
            checkBoxOpenClose.Visibility = Visibility.Collapsed;

            if (Device != null)
            {
                this.NameLabel.Content = Device.Name;

                checkBoxOnOff.IsChecked = Device.IsOn;

                if (Device is IRangeable)
                {
                    Controls.RowDefinitions[2].Height = new GridLength(30);
                    sliderRange.Visibility = Visibility.Visible;
                    sliderRange.Minimum = ((IRangeable)Device).MinValue;
                    sliderRange.Maximum = ((IRangeable)Device).MaxValue;
                    sliderRange.Value = ((IRangeable)Device).Value;
                }

                if (Device is IColorable)
                {
                    Controls.RowDefinitions[3].Height = new GridLength(30);
                    comboBoxColors.Visibility = Visibility.Visible;
                    comboBoxColors.SelectedItem = ((IColorable)Device).Color;
                }

                if (Device is ITwoStatable)
                {
                    Controls.RowDefinitions[4].Height = new GridLength(30);
                    checkBoxOpenClose.Visibility = Visibility.Visible;
                    checkBoxOpenClose.IsChecked = ((ITwoStatable)Device).IsOpen;
                }
            }
        }

        private void checkBoxOnOff_Checked(object sender, RoutedEventArgs e)
        {
            Device.On();
        }

        private void checkBoxOnOff_Unchecked(object sender, RoutedEventArgs e)
        {
            Device.Off();
        }

        private void comboBoxColors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Device is IColorable)
            {
                ((IColorable)Device).Color = (System.Drawing.Color)comboBoxColors.SelectedItem;
            }
        }

        private void sliderRange_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Device is IRangeable)
            {
                ((IRangeable)Device).Value = sliderRange.Value;
            }
        }

        private void checkBoxOpenClose_Checked(object sender, RoutedEventArgs e)
        {
            if (Device is ITwoStatable)
            {
                ((ITwoStatable)Device).Open();
            }
        }

        private void checkBoxOpenClose_Unchecked(object sender, RoutedEventArgs e)
        {
            if (Device is ITwoStatable)
            {
                ((ITwoStatable)Device).Close();
            }
        }
    }
}
