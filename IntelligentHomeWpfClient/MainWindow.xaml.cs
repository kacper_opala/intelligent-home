﻿using Devices.Abstractions;
using Devices.Implementations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IntelligentHomeWpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<DeviceBase> ItemList { get; set; }

        public MainWindow()
        {
            ItemList = new ObservableCollection<DeviceBase>();
            InitializeComponent();

            ItemList.Add(new LED("LED w łazience"));
            ItemList.Add(new SimpleLights("Światła w przedpokoju"));
            ItemList.Add(new RangeableLights("Światła w salonie"));
            ItemList.Add(new Shades("Rolety"));
            ItemList.Add(new TwoStateGate("Brama"));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            DeviceBase device = button.DataContext as DeviceBase;
            DeviceWindow deviceWindow = new DeviceWindow(device);
            deviceWindow.ShowDialog();
            SetDeviceListItem(deviceWindow.Device);
        }

        private void SetDeviceListItem(DeviceBase device)
        {
            if (ItemList != null && ItemList.Count > 0)
            {
                var d = ItemList.FirstOrDefault(i => i.Id == device.Id);
                int idx = ItemList.IndexOf(d);
                if (d != null)
                    ItemList[idx] = device;
            }

            devicesList.ItemsSource = null;
            devicesList.ItemsSource = ItemList;
        }
    }
}
