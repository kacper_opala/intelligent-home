﻿using IntelligentHome.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelligentHome.Implementations
{
    public class Gate : DeviceBase
    {
        public bool IsOpen { get; private set; }

        public Gate(string name) : base()
        {
            this.Name = name;       
        }

        public void Open()
        {
            IsOpen = true;
        }

        public void Close()
        {
            IsOpen = false;
        }
    }
}
