﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelligentHome.Abstractions;

namespace IntelligentHome.Implementations
{
    public class Lights : DeviceBase, IRangeable
    {
        public Color Color { get; set; }

        private int minSteps = 0;
        private int maxSteps = 100;

        public int CurrentStep { get; private set; }

        public Lights(string name) : base(name)
        {
        }

        public void UpOneStep()
        {
            if (CurrentStep < maxSteps)
                CurrentStep += 1;
        }

        public void DownOneStep()
        {
            if (CurrentStep > minSteps)
                CurrentStep -= 1;
        }

        public void UpToEnd()
        {
            CurrentStep = maxSteps;
        }

        public void DownToEnd()
        {
            CurrentStep = minSteps;
        }
    }
}