﻿using IntelligentHome.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelligentHome.Implementations
{
    public class Shades : DeviceBase, IRangeable
    {
        private int minSteps = 0;
        private int maxSteps = 100;

        public int CurrentStep { get; private set; }

        public Shades(string name) : base(name)
        {
        }

        public void UpOneStep()
        {
            if (CurrentStep < maxSteps)
                CurrentStep += 1;   
        }

        public void DownOneStep()
        {
            if (CurrentStep > minSteps)
                CurrentStep -= 1;
        }

        public void UpToEnd()
        {
            CurrentStep = maxSteps;
        }

        public void DownToEnd()
        {
            CurrentStep = minSteps;
        }
    }
}
