﻿namespace IntelligentHome.Abstractions
{
    public interface IRangeable
    {
        int CurrentStep { get; }

        void DownOneStep();
        void DownToEnd();
        void UpOneStep();
        void UpToEnd();
    }
}