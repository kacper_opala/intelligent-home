﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelligentHome.Abstractions
{
    public abstract class DeviceBase
    {
        public string Name { get; set; }
        public bool State { get; private set; }

        public DeviceBase()
        {
            this.State = false;
        }

        public DeviceBase(string name) : base()
        {
            this.Name = name;
        }

        public void On()
        {
            this.State = true;

        }

        public void Off()
        {
            this.State = false;
        }
    }
}
