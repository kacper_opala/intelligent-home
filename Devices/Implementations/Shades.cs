﻿using Devices.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devices.Implementations
{
    public class Shades : DeviceBase, IRangeable
    {
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public double Value { get; set; }

        public override string State
        {
            get
            {
                return "Name: " + this.Name + ", State: " + this.IsOnString + ", Value: " + Math.Round(this.Value, 2);
            }
        }

        public Shades(string name) : base(name)
        {
            this.MinValue = 0;
            this.MaxValue = 1;
            this.Value = 0;
        }
    }
}
