﻿using Devices.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devices.Implementations
{
    public class SimpleLights : DeviceBase
    {
        public override string State
        {
            get
            {
                return "Name: " + this.Name + ", State: " + IsOnString;
            }
        }

        public SimpleLights(string name) : base(name)
        {
        }
    }
}
