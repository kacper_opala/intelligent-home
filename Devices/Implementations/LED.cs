﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Devices.Abstractions;

namespace Devices.Implementations
{
    public class LED : DeviceBase, IRangeable, IColorable
    {
        public Color Color { get; set; }

        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public double Value { get; set; }

        public override string State
        {
            get
            {
                return "Name: " + this.Name + ", State: " + IsOnString + ", Value: " + Math.Round(this.Value, 2) + ", Color: " + Color.Name;
            }
        }

        public LED(string name) : base(name)
        {
            this.Color = Color.White;
            this.MinValue = 0;
            this.MaxValue = 1;
            this.Value = 0;
        }
    }
}