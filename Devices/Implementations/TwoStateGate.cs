﻿using Devices.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devices.Implementations
{
    public class TwoStateGate : DeviceBase, ITwoStatable
    {
        public bool IsOpen { get; private set; }

        public string IsOpenString { get { return IsOpen ? "Opened" : "Closed"; } }

        public override string State
        {
            get
            {
                return "Name: " + this.Name + ", State: " + IsOnString + ", " + IsOpenString;
            }
        }

        public TwoStateGate(string name) : base(name)
        {
        }

        public void Open()
        {
            IsOpen = true;
        }

        public void Close()
        {
            IsOpen = false;
        }
    }
}
