﻿namespace Devices.Abstractions
{
    public interface IRangeable
    {
       double Value { get; set; }
       double MinValue { get; set; }
       double MaxValue { get; set; }
    }
}