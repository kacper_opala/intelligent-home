﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devices.Abstractions
{
    public abstract class DeviceBase
    {
        public static Random Rnd = new Random(DateTime.Now.Millisecond);
        public int Id { get; private set; }
        public string Name { get; set; }
        public bool IsOn { get; private set; }
        public virtual string IsOnString { get { return IsOn ? "On" : "Off"; } }
        public abstract string State { get; }

        public DeviceBase()
        {
            this.Id = DeviceBase.Rnd.Next();
            this.IsOn = false;
        }

        public DeviceBase(string name) : this()
        {
            this.Name = name;
        }

        public DeviceBase(int id, string name) : this(name)
        {
            this.Id = id;
            this.Name = name;
        }

        public void On()
        {
            this.IsOn = true;
        }

        public void Off()
        {
            this.IsOn = false;
        }
    }
}
