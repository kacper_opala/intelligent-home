﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devices.Abstractions
{
    public interface IColorable
    {
        Color Color { get; set; }
    }
}
