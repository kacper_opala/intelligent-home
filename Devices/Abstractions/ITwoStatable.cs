﻿namespace Devices.Abstractions
{
    public interface ITwoStatable
    {
        bool IsOpen { get; }

        void Close();
        void Open();
    }
}